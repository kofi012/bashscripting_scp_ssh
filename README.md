:taco:
# Bash scripting, SSH and SCP

This class will cover some very important topics including bash scripting `ssh` and `scp`

#### bash scripting
Bash scripting is the ability to declare type bash command that provision a machine on a file, and then run it against a machine.

Provisioning a machine includes:

- making files and directories
- editing/ configuring files
- Installing files
- Starting and stopping files
- Creating initi files
- Sending files and code over to computer (scp)

#### SSH
SSH - secure Shell - is very useful to securely log into a computer at a distance.
It allows us to open a terminal on said computer with shell.

We can then use all our bash knowledge to configure the machine.

Main command:

```bash
# remote login to a machine
# syntax ssh <option> <user>@machine.ip

# Example
$ ssh -i ~/.ssh/mykey.pem ubuntu@34.21.23.4
$ ssh -i ~/.ssh/mykey.pem thor@34.21.23.4
```
SSH also allows you to run commands remotely.

```bash
# ls in a remote machine
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 ls

# you can just add the command after the ssh user and machine

$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 ls demos
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 cat bad.txt

# Create files in the machine
¢ ssh -i ~/.ssh/ch9_shared.pe ubuntu@34.245.2.98 touch kofi.md
```
#### scp - Secure copy - Intro remote location

Imagine like 'mv' but with ssh keys and remote computers.

```bash

# Syntax
# scp -i ~/.ssh/ch9_shared.pem <source/file> <target>
# scp -i ~/.ssh/ch9_shared.pem <source/file> <user>@<ip>:<path/to/location>

$ scp -i ~/.ssh/ch9_shared.pem bash_script101.sh ubuntu@34.245.2.98:/user/ubuntu/kofi1/
